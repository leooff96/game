module("onet.NetAttributeInstance", package.seeall)

Object = require("onet.Object")
Change = require("onet.Change")

NetAttributeInstance = setmetatable( {}, Object )
NetAttributeInstance.__index = NetAttributeInstance
NetAttributeInstance.__type = "NetAttributeInstance"

NetAttributeInstance.Updated = 0

function NetAttributeInstance:new(NetAttributeObj, NetObjectObj)
	
	local self = setmetatable( {}, NetAttributeInstance )
	
	self.NetAttribute = NetAttributeObj
	self.NetObject		= NetObjectObj
	self.Value 			= self:Get()
	
	return self
	
end

function NetAttributeInstance:SetValue(Value)
	
	self.Value = Value
	
end

function NetAttributeInstance:GetValue()
	
	return self.Value
	
end

function NetAttributeInstance:Get()
	
	return self.NetAttribute:Get( self.NetObject )
	
end

function NetAttributeInstance:Set(Value)
	
	self:SetValue(Value)
	
	return self.NetAttribute:Set( self.NetObject, Value )
	
end

function NetAttributeInstance:ReceiveChange(Change)
	
	if not self.NetAttribute:GetProtected() then
		
		self:Set( Change:GetValue() )
		
	end
	
end

function NetAttributeInstance:UpdateChange(ChangeObj)
	
	self:SetValue( ChangeObj:GetValue() )
	self.Updated 	= love.timer.getTime()
	
end

function NetAttributeInstance:GetChange()
	
	local Time = love.timer.getTime()
	
	if Time - self.Updated >= self.NetAttribute:GetDelay() then
		
		local Value = self:Get()
		
		if self:GetValue() ~= Value then
			
			local ChangeObj = Change:new(Value)
			
			ChangeObj:SetAttribute( self.NetAttribute )
			
			return ChangeObj
			
		end
		
	end
	
end

return NetAttributeInstance