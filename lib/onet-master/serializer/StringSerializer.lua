module("onet.serializer.StringSerializer", package.seeall)

Serializer = require("onet.serializer.Serializer")

StringSerializer = Serializer:new()
StringSerializer.__index = StringSerializer
StringSerializer.__type = "StringSerializer"

StringSerializer.Code = 6
StringSerializer.Type = "string"

Serializer.Class[ StringSerializer.__type ] = StringSerializer

function StringSerializer:new(SerializerObj, MessageEncoderObj)
	
	local self = setmetatable( {}, StringSerializer )
	
	self.Serializer		= SerializerObj
	self.MessageEncoder 	= MessageEncoderObj
	
	return self
	
end

function StringSerializer:Serialize(String)
	
	local Length = String:len()
	local Byte1 = Length % 256
	local Byte2 = ( Length - Byte1 ) / 256
	
	return string.char(Byte1) .. string.char(Byte2) .. String
	
end

return StringSerializer