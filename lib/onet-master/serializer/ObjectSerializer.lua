module("onet.serializer.ObjectSerializer", package.seeall)

Serializer = require("onet.serializer.Serializer")

ObjectSerializer = setmetatable( {}, Serializer )
ObjectSerializer.__index = ObjectSerializer
ObjectSerializer.__type = "ObjectSerializer"

ObjectSerializer.Code = 5

Serializer.Class[ ObjectSerializer.__type ] = ObjectSerializer

function ObjectSerializer:new(SerializerObj, MessageEncoderObj)
	
	local self = setmetatable( {}, ObjectSerializer )
	
	self.Serializer		= SerializerObj
	self.MessageEncoder 	= MessageEncoderObj
	
	return self
	
end

function ObjectSerializer:Serialize(Object)
	
	local BooleanSerializer		= self.Serializer:GetSerializer("BooleanSerializer")
	local NumberSerializer		= self.Serializer:GetSerializer("NumberSerializer")
	
	if Object:onetGetRemote() then
		
		if self.Serializer:GetTargetPeer() == Object:onetGetRemote() then
			
			local SerialID			= NumberSerializer:Serialize(Object:onetGetID())
			local SerialRemote	= BooleanSerializer:Serialize(true)
			
			return SerialID .. SerialRemote
			
		end
		
	else
		
		if Object:onetGetID() == nil then
			
			self.Serializer:GetTargetPeer():SendObject(Object)
			
		end
		
		if Object:onetGetID() then
			
			local SerialID			= NumberSerializer:Serialize(Object:onetGetID())
			local SerialRemote	= BooleanSerializer:Serialize(false)
			
			return SerialID .. SerialRemote
			
		end
		
	end
	
	return nil
	
end

return ObjectSerializer