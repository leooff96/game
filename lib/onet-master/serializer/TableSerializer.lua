module("onet.serializer.TableSerializer", package.seeall)

Serializer = require("onet.serializer.Serializer")

TableSerializer = Serializer:new()
TableSerializer.__index = TableSerializer
TableSerializer.__type = "TableSerializer"

TableSerializer.Code = 7
TableSerializer.Type = "table"

Serializer.Class[ TableSerializer.__type ] = TableSerializer

function TableSerializer:new(SerializerObj, MessageEncoderObj)
	
	local self = setmetatable( {}, TableSerializer )
	
	self.Serializer		= SerializerObj
	self.MessageEncoder 	= MessageEncoderObj
	
	return self
	
end

function TableSerializer:Serialize(Table)
	
	local SerialCount = 0
	local SerialSuffix = ""
	
	for Key, Value in pairs(Table) do
		
		local SerialKey = self.Serializer:Serialize(Key)
		local SerialValue = self.Serializer:Serialize(Value)
		
		if SerialKey ~= nil and SerialValue ~= nil then
			
			SerialSuffix = SerialSuffix .. SerialKey .. SerialValue
			SerialCount = SerialCount + 1
			
		end
		
	end
	
	return string.char(SerialCount) .. SerialSuffix
	
end

return TableSerializer