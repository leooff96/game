module("onet.serializer.NilSerializer", package.seeall)

Serializer = require("onet.serializer.Serializer")

NilSerializer = setmetatable( {}, Serializer )
NilSerializer.__index = NilSerializer
NilSerializer.__type = "NilSerializer"

NilSerializer.Code = 3
NilSerializer.Type = "nil"

Serializer.Class[ NilSerializer.__type ] = NilSerializer

function NilSerializer:new(SerializerObj, MessageEncoderObj)
	
	local self = setmetatable( {}, NilSerializer )
	
	self.Serializer		= SerializerObj
	self.MessageEncoder 	= MessageEncoderObj
	
	return self
	
end

function NilSerializer:Serialize(Data)
	
	return ""
	
end

return NilSerializer