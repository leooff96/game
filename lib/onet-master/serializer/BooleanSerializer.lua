module("onet.serializer.BooleanSerializer", package.seeall)

Serializer = require("onet.serializer.Serializer")

BooleanSerializer = setmetatable( {}, Serializer )
BooleanSerializer.__index = BooleanSerializer
BooleanSerializer.__type = "BooleanSerializer"

BooleanSerializer.Code = 1
BooleanSerializer.Type = "boolean"

Serializer.Class[ BooleanSerializer.__type ] = BooleanSerializer

function BooleanSerializer:new(SerializerObj, MessageEncoderObj)
	
	local self = setmetatable( {}, BooleanSerializer )
	
	self.Serializer		= SerializerObj
	self.MessageEncoder 	= MessageEncoderObj
	
	return self
	
end

function BooleanSerializer:Serialize(Boolean)
	
	return string.char( Boolean and 1 or 0 )
	
end

return BooleanSerializer