module("onet.serializer.ChangeSerializer", package.seeall)

Serializer = require("onet.serializer.Serializer")

ChangeSerializer = setmetatable( {}, Serializer )
ChangeSerializer.__index = ChangeSerializer
ChangeSerializer.__type = "ChangeSerializer"

ChangeSerializer.Code = 2
ChangeSerializer.Type = "Change"

Serializer.Class[ ChangeSerializer.__type ] = ChangeSerializer

function ChangeSerializer:new(SerializerObj, MessageEncoderObj)
	
	local self = setmetatable( {}, ChangeSerializer )
	
	self.Serializer		= SerializerObj
	self.MessageEncoder 	= MessageEncoderObj
	
	return self
	
end

function ChangeSerializer:Serialize(ChangeObj)
	
	return self.Serializer:Serialize( ChangeObj:GetValue() )
	
end

return ChangeSerializer