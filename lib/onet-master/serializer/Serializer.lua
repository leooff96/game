module("onet.serializer.Serializer", package.seeall)

Object = require("onet.Object")
NetObject = require("onet.NetObject")

Serializer = setmetatable( {}, Object )
Serializer.__index = Serializer
Serializer.__type = "Serializer"

Serializer.Class = {}

function Serializer:new(MessageEncoderObj)
	
	local self = setmetatable( {}, Serializer )
	
	self.MessageEncoder = MessageEncoderObj
	self.Serializer = {}
	self.SerializerType = {}
	
	for _, SerializerClass in pairs(self.Class) do
		
		self.SerializerType[ SerializerClass:type() ] = SerializerClass:new(self, MessageEncoderObj)
		
		if SerializerClass:GetType() then
			
			self.Serializer[ SerializerClass:GetType() ] = self.SerializerType[ SerializerClass:type() ]
			
		end
		
	end
	
	return self
	
end

function Serializer:Serialize(Object)
	
	local ObjectType = type(Object)
	
	if ObjectType == "table" then
		
		if type(Object.type) == "function" then
			
			local Type = Object:type()
			local SerializerClass = self.Serializer[Type]
			
			if SerializerClass then
				
				local Serial = SerializerClass:Serialize(Object)
				
				if Serial then
					
					return string.char(SerializerClass:GetCode()) .. Serial
					
				end
				
			end
			
			if Object:typeOf(NetObject) then
				
				local Serial = self.SerializerType.ObjectSerializer:Serialize(Object)
				
				if Serial then
					
					return string.char(self.SerializerType.ObjectSerializer:GetCode()) .. Serial
					
				end
				
			end
			
			return nil
			
		end
		
		return string.char(self.Serializer["table"]:GetCode()) .. self.Serializer["table"]:Serialize(Object)
		
	elseif ObjectType == "userdata" then
		
		if type(rawget(Object, "type")) == "function" then
			
			local Type = Object:type()
			local SerializerClass = self.Serializer[Type]
			
			if SerializerClass then
				
				return string.char(SerializerClass:GetCode()) .. SerializerClass:Serialize(Object)
				
			end
			
		end
		
	end
	
	local SerializerClass = self.Serializer[ObjectType]
	
	if SerializerClass then
		
		return string.char(SerializerClass:GetCode()) .. SerializerClass:Serialize(Object)
		
	end
	
end

function Serializer:GetSerializer(Name)
	
	return self.SerializerType[Name]
	
end

function Serializer:GetCode()
	
	return self.Code
	
end

function Serializer:GetType()
	
	return self.Type
	
end

function Serializer:SetTargetPeer(TargetPeer)
	
	self.TargetPeer = TargetPeer
	
end

function Serializer:GetTargetPeer()
	
	return self.TargetPeer
	
end

return Serializer