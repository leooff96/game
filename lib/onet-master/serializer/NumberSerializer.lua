module("onet.serializer.NumberSerializer", package.seeall)

Serializer = require("onet.serializer.Serializer")

NumberSerializer = Serializer:new()
NumberSerializer.__index = NumberSerializer
NumberSerializer.__type = "NumberSerializer"

NumberSerializer.Code = 4
NumberSerializer.Type = "number"

Serializer.Class[ NumberSerializer.__type ] = NumberSerializer

function NumberSerializer:new(SerializerObj, MessageEncoderObj)
	
	local self = setmetatable( {}, NumberSerializer )
	
	self.Serializer		= SerializerObj
	self.MessageEncoder 	= MessageEncoderObj
	
	return self
	
end

function NumberSerializer:Serialize(Number)
	
	local Bit = {}
	local n = Number
	
	if Number < 0 then
		
		Number = -Number
		Bit[1] = true
		
	end
	
	if Number > 0 then
		
		local Exponent = math.floor( math.log10(Number) / math.log10(2) + 1 )
		local Fraction = Number / ( 2 ^ Exponent )
		
		local Power = 2048
		
		Exponent = Exponent + 1024
		
		for i = 12, 2, -1 do
			
			Power = Power * 0.5
			
			if Exponent >= Power then
				
				Exponent = Exponent - Power
				Bit[i] 	= true
				
			end
			
		end
		
		for i = 13, 64 do
			
			Fraction = Fraction * 2
			
			if Fraction >= 1 then
				
				Fraction = Fraction - 1
				Bit[i] 	= true
				
			end
			
		end
		
	end
	
	local Serial = ""
	
	for i = 1, 64, 8 do
		
		local Byte = 0
		local Power = 1
		
		for j = 1, 8 do
			
			if Bit[i + j - 1] then
				
				Byte = Byte + Power
				
			end
			
			Power = Power * 2
			
		end
		
		Serial = Serial .. string.char(Byte)
		
	end
	
	return Serial
	
end

return NumberSerializer