module("onet.NetObject", package.seeall)

Object = require("onet.Object")
Change = require("onet.Change")
PeerList = require("onet.PeerList")
Util = require("onet.Util")

table = Util.table

NetObject = setmetatable( {}, Object )
NetObject.__index = NetObject
NetObject.__type = "NetObject"

NetObject.onetAttributes 			= {}		-- {["name"] = NetAttribute:new("name"), ["name2"] = NetAttribute:new("name2")}
NetObject.onetMethods				= {}
NetObject.onetExposedMethods		= {}
NetObject.onetRemote 				= false
NetObject.onetID 						= nil

NetObject.onetCreateChannel		= 0
NetObject.onetCreateReliable		= true
NetObject.onetCreateSequenced		= true

NetObject.onetDestroyChannel		= 0
NetObject.onetDestroyReliable		= false
NetObject.onetDestroySequenced	= false

function NetObject:new()
	-- Just in case that you wanted to use the default constructor
	
	local self = setmetatable( {}, self )
	
	self:onetInit()
	
	return self
	
end

function NetObject:__gc()
	
	return self:onetDestroy()
	
end

function NetObject:onetNew(...)
	
	return self:onetNewFrom( {}, ... )
	
end

function NetObject:onetNewFrom(Object, ...)
	
	local self = setmetatable( Object, self )
	
	self:onetConstruct(...)
	
	return self
	
end

function NetObject:onetConstruct(Values)
	
	self:onetInit()
	self:onetSetValues(Values)
	
end

function NetObject:onetInit()
	
	self.onetValues 	= {}
	self.onetPeers 	= PeerList:new()
	
	for Index, NetAttributeObj in pairs(self.onetAttributes) do
		
		self.onetValues[Index] = NetAttributeObj:GetInstance(self)
		
	end
	
end

function NetObject:onetGetAllValues()
	
	local Values = {}
	
	for Index, NetAttributeInstanceObj in pairs(self.onetValues) do
		
		Values[Index] = NetAttributeInstanceObj:Get()
		
	end
	
	return Values
	
end

function NetObject:onetSetValues(Values)
	
	for Index, Value in pairs(Values) do
		
		local NetAttributeInstanceObj = self.onetValues[Index]
		
		if NetAttributeInstanceObj then
			
			NetAttributeInstanceObj:Set( Value )
			
		end
		
	end
	
end

function NetObject:onetFetchChanges()
	
	local Changes = {}
	
	for Index, NetAttributeInstanceObj in pairs(self.onetValues) do
		
		Changes[Index] = NetAttributeInstanceObj:GetChange()
		
	end
	
	return Changes
	
end

function NetObject:onetUpdateChanges(Changes)
	
	for Index, Change in pairs(Changes) do
		
		local NetAttributeInstanceObj = self.onetValues[Index]
		
		if NetAttributeInstanceObj then
			
			NetAttributeInstanceObj:UpdateChange( Change )
			
		end
		
	end
	
end

function NetObject:onetReceiveChanges(Changes)
	-- Method called remotely
	for Index, Change in pairs(Changes) do
		
		local NetAttributeInstanceObj = self.onetValues[Index]
		
		if NetAttributeInstanceObj then
			
			NetAttributeInstanceObj:ReceiveChange( Change:GetValue() )
			
		end
		
	end
	
end

function NetObject:onetAllowAccess(Peer)
	
	if not self.onetRemote then
		
		self.onetPeers:Put(Peer)
		
	end
	
end

function NetObject:onetDisallowAccess(Peer)
	
	if not self.onetRemote then
		
		self.onetPeers:Remove(Peer)
		
	end
	
end

function NetObject:onetMethod(Name, ...)
	
	local NetMethodObj = self.onetMethods[Name]
	
	if NetMethodObj then
		
		local Remote = self:onetGetRemote()
		
		if Remote then
			
			Remote:GetServer():GetMessageEncoder():CallMethod(
				Remote,
				self,
				Name,
				NetMethodObj:GetChannel(),
				NetMethodObj:GetReliable(),
				NetMethodObj:GetSequenced(),
				...
			)
			
		else
			
			for Address, Peer in self.onetPeers:GetPeerEnumerator() do
				
				Peer:GetServer():GetMessageEncoder():CallMethod(
					Peer,
					self,
					Name,
					NetMethodObj:GetChannel(),
					NetMethodObj:GetReliable(),
					NetMethodObj:GetSequenced(),
					...
				)
				
			end
			
		end
		
	end
	
end

function NetObject:onetRequestCallMethod(Name, Params)
	
	if table.contains(self.onetExposedMethods, Name) then
		
		local Method = self[Name]
		
		if Method then
			
			Method(self, unpack(Params))
			
		end
		
	end
	
end

function NetObject:onetDestroy()
	
	if not self:onetGetRemote() then
		
		if self.onetObjectManager then
			
			for Address, Peer in self.onetPeers:GetPeerEnumerator() do
				
				Peer:GetServer():GetMessageEncoder():DestroyObject(Peer, self)
				
			end
			
			self.onetObjectManager:RemoveObject( self.onetID )
			self.onetID = nil
			self.onetObjectManager = nil
			self.onetPeers = PeerList:new()
			
		end
		
	end
	
end

function NetObject:onetSetRemote(Remote)
	
	self.onetRemote = Remote
	
end

function NetObject:onetGetRemote()
	
	return self.onetRemote
	
end

function NetObject:onetSetID(ID)
	
	self.onetID = ID
	
end

function NetObject:onetGetID()
	
	return self.onetID
	
end

function NetObject:onetSetCreateChannel(CreateChannel)
	
	self.onetCreateChannel = CreateChannel
	
end

function NetObject:onetGetCreateChannel()
	
	return self.onetCreateChannel
	
end

function NetObject:onetSetCreateReliable(CreateReliable)
	
	self.onetCreateReliable = CreateReliable
	
end

function NetObject:onetGetCreateReliable()
	
	return self.onetCreateReliable
	
end

function NetObject:onetSetCreateSequenced(CreateSequenced)
	
	self.onetCreateSequenced = CreateSequenced
	
end

function NetObject:onetGetCreateSequenced()
	
	return self.onetCreateSequenced
	
end

function NetObject:onetSetDestroyChannel(DestroyChannel)
	
	self.onetDestroyChannel = DestroyChannel
	
end

function NetObject:onetGetDestroyChannel()
	
	return self.onetDestroyChannel
	
end

function NetObject:onetSetDestroyReliable(DestroyReliable)
	
	self.onetDestroyReliable = DestroyReliable
	
end

function NetObject:onetGetDestroyReliable()
	
	return self.onetDestroyReliable
	
end

function NetObject:onetSetDestroySequenced(DestroySequenced)
	
	self.onetDestroySequenced = DestroySequenced
	
end

function NetObject:onetGetDestroySequenced()
	
	return self.onetDestroySequenced
	
end

function NetObject:onetSetObjectManager(ObjectManager)
	
	self.onetObjectManager = ObjectManager
	
end

function NetObject:onetGetObjectManager()
	
	return self.onetObjectManager
	
end

function NetObject:onetGetPeerList()
	
	return self.onetPeers
	
end

return NetObject