module("onet.Object", package.seeall)

Object = {}
Object.__index = Object
Object.__type = "Object"

function Object:typeOf(Class)
	
	local Metatable = getmetatable(self)
	
	while Metatable do
		
		if Metatable == Class or Metatable.__type == Class then
			
			return true
			
		end
		
		Metatable = getmetatable(Metatable)
		
	end
	
	return false
	
end

function Object:type()
	
	return self.__type
	
end

return Object