module("onet.deserializer.BooleanDeserializer", package.seeall)

Deserializer = require("onet.deserializer.Deserializer")
BooleanSerializer = require("onet.serializer.BooleanSerializer")

BooleanDeserializer = setmetatable( {}, Deserializer )
BooleanDeserializer.__index = BooleanDeserializer
BooleanDeserializer.__type = "BooleanDeserializer"

Deserializer.Of[ BooleanSerializer:GetCode() ] = BooleanDeserializer

function BooleanDeserializer:new(DeserializerObj, MessageDecoderObj)
	
	local self = setmetatable( {}, BooleanDeserializer )
	
	self.Deserializer		= DeserializerObj
	self.MessageDecoder	= MessageDecoderObj
	
	return self
	
end

function BooleanDeserializer:Deserialize(Data)
	
	return Data:byte(1) == 1, Data:sub(2)
	
end

return BooleanDeserializer