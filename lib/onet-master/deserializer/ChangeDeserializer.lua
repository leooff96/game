module("onet.deserializer.ChangeDeserializer", package.seeall)

Change = require("onet.Change")

Deserializer = require("onet.deserializer.Deserializer")
ChangeSerializer = require("onet.serializer.ChangeSerializer")

ChangeDeserializer = setmetatable( {}, Deserializer )
ChangeDeserializer.__index = ChangeDeserializer
ChangeDeserializer.__type = "ChangeDeserializer"

Deserializer.Of[ ChangeSerializer:GetCode() ] = ChangeDeserializer

function ChangeDeserializer:new(DeserializerObj, MessageDecoderObj)
	
	local self = setmetatable( {}, ChangeDeserializer )
	
	self.Deserializer		= DeserializerObj
	self.MessageDecoder	= MessageDecoderObj
	
	return self
	
end

function ChangeDeserializer:Deserialize(Data)
	
	local Value, Data = self.Deserializer:Deserialize(Data)
	local ChangeObj = Change:new(Value)
	
	return ChangeObj, Data
	
end

return ChangeDeserializer