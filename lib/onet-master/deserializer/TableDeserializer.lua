module("onet.deserializer.TableDeserializer", package.seeall)

Deserializer = require("onet.deserializer.Deserializer")
TableSerializer = require("onet.serializer.TableSerializer")

TableDeserializer = Deserializer:new()
TableDeserializer.__index = TableDeserializer
TableDeserializer.__type = "TableDeserializer"

Deserializer.Of[ TableSerializer:GetCode() ] = TableDeserializer

function TableDeserializer:new(DeserializerObj, MessageDecoderObj)
	
	local self = setmetatable( {}, TableDeserializer )
	
	self.Deserializer		= DeserializerObj
	self.MessageDecoder	= MessageDecoderObj
	
	return self
	
end

function TableDeserializer:Deserialize(Data)
	
	local Count = Data:byte(1)
	local Table = {}
	
	Data = Data:sub(2)
	
	for i = 1, Count do
		
		local Index
		local Value
		
		Index, Data = self.Deserializer:Deserialize(Data)
		Value, Data = self.Deserializer:Deserialize(Data)
		
		if Index ~= nil and Value ~= nil then
			
			Table[Index] = Value
			
		end
		
	end
	
	return Table
	
end

return TableDeserializer