module("onet.deserializer.ObjectDeserializer", package.seeall)

Deserializer = require("onet.deserializer.Deserializer")
ObjectSerializer = require("onet.serializer.ObjectSerializer")

ObjectDeserializer = setmetatable( {}, Deserializer )
ObjectDeserializer.__index = ObjectDeserializer
ObjectDeserializer.__type = "ObjectDeserializer"

Deserializer.Of[ ObjectSerializer:GetCode() ] = ObjectDeserializer

function ObjectDeserializer:new(DeserializerObj, MessageDecoderObj)
	
	local self = setmetatable( {}, ObjectDeserializer )
	
	self.Deserializer		= DeserializerObj
	self.MessageDecoder	= MessageDecoderObj
	
	return self
	
end

function ObjectDeserializer:Deserialize(Data)
	
	local NumberDeserializer		= self.Deserializer:GetDeserializer("NumberDeserializer")
	local BooleanDeserializer		= self.Deserializer:GetDeserializer("BooleanDeserializer")
	
	local ID,		Data		= NumberDeserializer:Deserialize(Data)
	local Remote,	Data		= BooleanDeserializer:Deserialize(Data)
	
	if Remote then
		
		return self.MessageDecoder:GetServer():GetObjectManager():GetTempObject(ID), Data
		
	end
	
	return self.Deserializer:GetSourcePeer():GetObjectManager():GetTempObject(ID), Data
	
end

return ObjectDeserializer