module("onet.deserializer.NilDeserializer", package.seeall)

Deserializer = require("onet.deserializer.Deserializer")
NilSerializer = require("onet.serializer.NilSerializer")

NilDeserializer = setmetatable( {}, Deserializer )
NilDeserializer.__index = NilDeserializer
NilDeserializer.__type = "NilDeserializer"

Deserializer.Of[ NilSerializer:GetCode() ] = NilDeserializer

function NilDeserializer:new(DeserializerObj, MessageDecoderObj)
	
	local self = setmetatable( {}, NilDeserializer )
	
	self.Deserializer		= DeserializerObj
	self.MessageDecoder	= MessageDecoderObj
	
	return self
	
end

function NilDeserializer:Deserialize(Data)
	
	return nil, Data
	
end

return NilDeserializer