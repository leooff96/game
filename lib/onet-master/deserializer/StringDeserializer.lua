module("onet.deserializer.StringDeserializer", package.seeall)

Deserializer = require("onet.deserializer.Deserializer")
StringSerializer = require("onet.serializer.StringSerializer")

StringDeserializer = Deserializer:new()
StringDeserializer.__index = StringDeserializer
StringDeserializer.__type = "StringDeserializer"

Deserializer.Of[ StringSerializer:GetCode() ] = StringDeserializer

function StringDeserializer:new(DeserializerObj, MessageDecoderObj)
	
	local self = setmetatable( {}, StringDeserializer )
	
	self.Deserializer		= DeserializerObj
	self.MessageDecoder	= MessageDecoderObj
	
	return self
	
end

function StringDeserializer:Deserialize(Data)
	
	local Byte1 = Data:byte(1)
	local Byte2 = Data:byte(2)
	local Length = Byte1 + Byte2 * 256; Data = Data:sub(3)
	local String = Data:sub(1, Length)
	
	return String, Data:sub(Length + 1)
	
end

return StringDeserializer