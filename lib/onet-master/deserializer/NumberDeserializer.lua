module("onet.deserializer.NumberDeserializer", package.seeall)

Deserializer = require("onet.deserializer.Deserializer")
NumberSerializer = require("onet.serializer.NumberSerializer")

NumberDeserializer = Deserializer:new()
NumberDeserializer.__index = NumberDeserializer
NumberDeserializer.__type = "NumberDeserializer"

Deserializer.Of[ NumberSerializer:GetCode() ] = NumberDeserializer

function NumberDeserializer:new(DeserializerObj, MessageDecoderObj)
	
	local self = setmetatable( {}, NumberDeserializer )
	
	self.Deserializer		= DeserializerObj
	self.MessageDecoder	= MessageDecoderObj
	
	return self
	
end

function NumberDeserializer:Deserialize(Data)
	
	if Data:len() < 8 then
		
		return nil, Data
		
	end
	
	local Bit = {}
	
	for i = 1, 64, 8 do
		
		local Byte = Data:byte(1)
		local Power = 128
		
		for j = 8, 1, -1 do
			
			if Byte >= Power then
				
				Byte 				= Byte - Power
				Bit[i + j - 1] = true
				
			end
			
			Power = Power * 0.5
			
		end
		
		Data = Data:sub(2)
		
	end
	
	local Exponent = -1024
	
	do
		
		local ExponentPower = 1
		
		for i = 2, 12 do
			
			if Bit[i] then
				
				Exponent = Exponent + ExponentPower
				
			end
			
			ExponentPower = ExponentPower * 2
			
		end
		
	end
	
	local Fraction = 0
	
	do
		
		local FractionPower = 1
		
		for i = 13, 64 do
			
			FractionPower = FractionPower * 0.5
			
			if Bit[i] then
				
				Fraction = Fraction + FractionPower
				
			end
			
		end
		
	end
	
	return Fraction * ( 2 ^ Exponent ), Data
	
end

return NumberDeserializer