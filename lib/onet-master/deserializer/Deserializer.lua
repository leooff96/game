module("onet.deserializer.Deserializer", package.seeall)

Object = require("onet.Object")

Deserializer = setmetatable( {}, Object )
Deserializer.__index = Deserializer
Deserializer.__type = "Deserializer"

Deserializer.Of = {}

function Deserializer:new(MessageDecoderObj)
	
	local self = setmetatable( {}, Deserializer )
	
	self.MessageDecoder		= MessageDecoderObj
	self.Deserializer			= {}
	self.DeserializerType	= {}
	
	for Code, DeserializerClass in pairs(self.Of) do
		
		self.DeserializerType[ DeserializerClass.__type ]	= DeserializerClass:new(self, MessageDecoderObj)
		self.Deserializer[ Code ]									= self.DeserializerType[ DeserializerClass.__type ]
		
	end
	
	return self
	
end

function Deserializer:Deserialize(Data)
	
	while Data:len() > 0 do
		
		local Code = Data:byte(1)
		local DeserializerClass = self.Deserializer[Code]
		
		Data = Data:sub(2)
		
		if DeserializerClass then
			
			return DeserializerClass:Deserialize(Data)
			
		end
		
		return nil
		
	end
	
end

function Deserializer:GetDeserializer(Type)
	
	return self.DeserializerType[Type]
	
end

function Deserializer:SetSourcePeer(SourcePeer)
	
	self.SourcePeer = SourcePeer
	
end

function Deserializer:GetSourcePeer()
	
	return self.SourcePeer
	
end

return Deserializer