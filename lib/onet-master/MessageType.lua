module("onet.MessageType", package.seeall)

MessageType = {
	CREATE_OBJECT = 1,
	UPDATE_OBJECT = 2,
	DESTROY_OBJECT = 3,
	CALL_METHOD = 4,
}

return MessageType