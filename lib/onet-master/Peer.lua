module("onet.Peer", package.seeall)

Object 				= require("onet.Object")

NetObjectManager 	= require("onet.NetObjectManager")
NetObject			= require("onet.NetObject")
Util 					= require("onet.Util")

Peer = setmetatable( {}, Object )
Peer.__index = Peer
Peer.__type = "Peer"

Peer.Connected = false

function Peer:new(enetPeer)
	
	local self = setmetatable( {}, Peer )
	
	self.Peer 				= enetPeer
	self.MessageQueue		= {}
	
	self.ObjectManager	= NetObjectManager:new()
	self.ObjectManager:SetRemote(self)
	
	return self
	
end

function Peer:GetObjectManager()
	
	return self.ObjectManager
	
end

function Peer:GetAddress()
	
	return tostring(self.Peer)
	
end

function Peer:SetPeer(Peer)
	
	self.Peer = Peer
	
end

function Peer:GetPeer()
	
	return self.Peer
	
end

function Peer:SetServer(Server)
	
	self.Server = Server
	
end

function Peer:GetServer()
	
	return self.Server
	
end

function Peer:Close()
	
	self.Peer:disconnect_later()
	
end

function Peer:GetConnected()
	
	return self.Connected
	
end

function Peer:OnConnect()
	
	self.Connected = true
	
	for Index, Message in pairs(self.MessageQueue) do
		
		self:Send( unpack(Message) )
		
	end
	
	self.MessageQueue = nil
	
end

function Peer:OnDisconnect()
	
	self.MessageQueue = {}
	self.Connected = false
	
end

function Peer:OnReceive(Data)
	
	self.Server:GetMessageDecoder():Decode(self, Data)
	
end

function Peer:SendObject(Object)
	
	if Object:typeOf(NetObject) then
		
		self.Server:GetMessageEncoder():SendObject(self, Object)
		Object:onetAllowAccess(self)
		
		return true
		
	end
	
	return false
	
end

function Peer:Send(Data, Channel, Reliable, Sequenced)
	
	if not self.Connected then
		
		self.MessageQueue[#self.MessageQueue + 1] = {Data, Channel, Reliable, Sequenced}
		
	else
		
		self.Peer:send(Data, Channel, Reliable and "reliable" or Sequenced and "unreliable" or "unsequenced")
		
	end
	
end

return Peer