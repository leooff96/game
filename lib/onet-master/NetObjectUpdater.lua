module("onet.NetObjectUpdater", package.seeall)

Object = require("onet.Object")
Table = require("onet.Table")

NetObjectUpdater = setmetatable( {}, Object )
NetObjectUpdater.__index = NetObjectUpdater
NetObjectUpdater.__type = "NetObjectUpdater"

function NetObjectUpdater:new(ServerObj)
	
	local self = setmetatable( {}, NetObjectUpdater )
	
	self.Server				= ServerObj
	self.PeerList			= self.Server:GetPeerList()
	self.ObjectManager	= self.Server:GetObjectManager()
	
	return self
	
end

function NetObjectUpdater:Update()
	
	for ID, Object in self.ObjectManager:GetObjectEnumerator() do
		
		self:CheckUpdate(Object)
		
	end
	
	for Address, Peer in self.PeerList:GetPeerEnumerator() do
		
		for ID, Object in Peer:GetObjectManager():GetObjectEnumerator() do
			
			self:CheckUpdate(Object)
			
		end
		
	end
	
end

function NetObjectUpdater:CheckUpdate(Object)
	
	local Changes = Object:onetFetchChanges()
	local ReliableChanges = {}
	local SequencedChanges = {}
	local UnreliableChanges = {}
	local MessageEncoder = self.Server:GetMessageEncoder()
	
	Object:onetUpdateChanges(Changes)
	
	if Object:typeOf(Table) then
		
		if Object:onetGetAttributeReliable() then
			
			ReliableChanges[Object:onetGetAttributeChannel()] = Changes
			
		elseif Object:onetGetAttributeSequenced() then
			
			SequencedChanges[Object:onetGetAttributeChannel()] = Changes
			
		else
			
			UnreliableChanges[Object:onetGetAttributeChannel()] = Changes
			
		end
		
	else
		
		for Name, Change in pairs(Changes) do
			
			local Attribute = Change:GetAttribute()
			local Channel = Attribute:GetChannel()
			local ChannelTable
			
			if Attribute:GetReliable() then
				
				ChannelTable = ReliableChanges[Channel]
				
				if not ChannelTable then
					
					ChannelTable = {}
					
					ReliableChanges[Channel] = ChannelTable
					
				end
				
			elseif Attribute:GetSequenced() then
				
				ChannelTable = SequencedChanges[Channel]
				
				if not ChannelTable then
					
					ChannelTable = {}
					
					SequencedChanges[Channel] = ChannelTable
					
				end
				
			else
				
				ChannelTable = UnreliableChanges[Channel]
				
				if not ChannelTable then
					
					ChannelTable = {}
					
					UnreliableChanges[Channel] = ChannelTable
					
				end
				
			end
			
			ChannelTable[Name] = Change
			
		end
		
	end
	
	local RemotePeer = Object:onetGetRemote()
	
	if next(ReliableChanges) then
		
		for Channel, Changes in pairs(ReliableChanges) do
		
			for Address, Peer in Object:onetGetPeerList():GetPeerEnumerator() do
				
				MessageEncoder:UpdateObject(Peer, Object, Changes, Channel, true, true)
				
			end
			
			if RemotePeer then
				
				MessageEncoder:UpdateObject(RemotePeer, Object, Changes, Channel, true, true)
				
			end
			
		end
		
	end
	
	if next(SequencedChanges) then
		
		for Channel, Changes in pairs(SequencedChanges) do
			
			for Address, Peer in Object:onetGetPeerList():GetPeerEnumerator() do
				
				MessageEncoder:UpdateObject(Peer, Object, Changes, Channel, false, true)
				
			end
			
			if RemotePeer then
				
				MessageEncoder:UpdateObject(RemotePeer, Object, Changes, Channel, false, true)
				
			end
			
		end
		
	end
	
	if next(UnreliableChanges) then
		
		for Channel, Changes in pairs(UnreliableChanges) do
			
			for Address, Peer in Object:onetGetPeerList():GetPeerEnumerator() do
				
				MessageEncoder:UpdateObject(Peer, Object, Changes, Channel, false, false)
				
			end
			
			if RemotePeer then
				
				MessageEncoder:UpdateObject(RemotePeer, Object, Changes, Channel, false, false)
				
			end
			
		end
		
	end
	
end

return NetObjectUpdater