local RPath = ...
local Path 	= RPath:gsub("%.", "/")

ONet = {}

package.loaded["onet"] = ONet
package.loaded["onet.enet"]							=		require("enet")
package.preload["onet.Change"]						=		assert(love.filesystem.load(Path.."/Change.lua"))
package.preload["onet.MessageDecoder"]				=		assert(love.filesystem.load(Path.."/MessageDecoder.lua"))
package.preload["onet.MessageEncoder"]				=		assert(love.filesystem.load(Path.."/MessageEncoder.lua"))
package.preload["onet.MessageType"]					=		assert(love.filesystem.load(Path.."/MessageType.lua"))
package.preload["onet.NetAttribute"]				=		assert(love.filesystem.load(Path.."/NetAttribute.lua"))
package.preload["onet.NetAttributeInstance"]		=		assert(love.filesystem.load(Path.."/NetAttributeInstance.lua"))
package.preload["onet.NetMethod"]					=		assert(love.filesystem.load(Path.."/NetMethod.lua"))
package.preload["onet.NetObject"]					=		assert(love.filesystem.load(Path.."/NetObject.lua"))
package.preload["onet.NetObjectManager"]			=		assert(love.filesystem.load(Path.."/NetObjectManager.lua"))
package.preload["onet.NetObjectUpdater"]			=		assert(love.filesystem.load(Path.."/NetObjectUpdater.lua"))
package.preload["onet.Object"]						=		assert(love.filesystem.load(Path.."/Object.lua"))
package.preload["onet.Peer"]							=		assert(love.filesystem.load(Path.."/Peer.lua"))
package.preload["onet.PeerList"]						=		assert(love.filesystem.load(Path.."/PeerList.lua"))
package.preload["onet.Server"]						=		assert(love.filesystem.load(Path.."/Server.lua"))
package.preload["onet.Table"]							=		assert(love.filesystem.load(Path.."/Table.lua"))
package.preload["onet.Util"]							=		assert(love.filesystem.load(Path.."/Util.lua"))

package.preload["onet.serializer.Serializer"]			=		assert(love.filesystem.load(Path.."/serializer/Serializer.lua"))
package.preload["onet.serializer.BooleanSerializer"]	=		assert(love.filesystem.load(Path.."/serializer/BooleanSerializer.lua"))
package.preload["onet.serializer.ChangeSerializer"]	=		assert(love.filesystem.load(Path.."/serializer/ChangeSerializer.lua"))
package.preload["onet.serializer.NilSerializer"]		=		assert(love.filesystem.load(Path.."/serializer/NilSerializer.lua"))
package.preload["onet.serializer.NumberSerializer"]	=		assert(love.filesystem.load(Path.."/serializer/NumberSerializer.lua"))
package.preload["onet.serializer.ObjectSerializer"]	=		assert(love.filesystem.load(Path.."/serializer/ObjectSerializer.lua"))
package.preload["onet.serializer.StringSerializer"]	=		assert(love.filesystem.load(Path.."/serializer/StringSerializer.lua"))
package.preload["onet.serializer.TableSerializer"]		=		assert(love.filesystem.load(Path.."/serializer/TableSerializer.lua"))

package.preload["onet.deserializer.Deserializer"]			=		assert(love.filesystem.load(Path.."/deserializer/Deserializer.lua"))
package.preload["onet.deserializer.BooleanDeserializer"]	=		assert(love.filesystem.load(Path.."/deserializer/BooleanDeserializer.lua"))
package.preload["onet.deserializer.ChangeDeserializer"]	=		assert(love.filesystem.load(Path.."/deserializer/ChangeDeserializer.lua"))
package.preload["onet.deserializer.NilDeserializer"]		=		assert(love.filesystem.load(Path.."/deserializer/NilDeserializer.lua"))
package.preload["onet.deserializer.NumberDeserializer"]	=		assert(love.filesystem.load(Path.."/deserializer/NumberDeserializer.lua"))
package.preload["onet.deserializer.ObjectDeserializer"]	=		assert(love.filesystem.load(Path.."/deserializer/ObjectDeserializer.lua"))
package.preload["onet.deserializer.StringDeserializer"]	=		assert(love.filesystem.load(Path.."/deserializer/StringDeserializer.lua"))
package.preload["onet.deserializer.TableDeserializer"]	=		assert(love.filesystem.load(Path.."/deserializer/TableDeserializer.lua"))

require("onet.deserializer.BooleanDeserializer")
require("onet.deserializer.ChangeDeserializer")
require("onet.deserializer.NilDeserializer")
require("onet.deserializer.NumberDeserializer")
require("onet.deserializer.ObjectDeserializer")
require("onet.deserializer.StringDeserializer")
require("onet.deserializer.TableDeserializer")

return ONet