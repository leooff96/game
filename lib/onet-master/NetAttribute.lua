module("onet.NetAttribute", package.seeall)

Object = require("onet.Object")
NetAttributeInstance = require("onet.NetAttributeInstance")

NetAttribute = setmetatable( {}, Object )
NetAttribute.__index = NetAttribute
NetAttribute.__type = "NetAttribute"

NetAttribute.Delay = 0
NetAttribute.Channel = 0
NetAttribute.Reliable = true
NetAttribute.Sequenced = true
NetAttribute.Protected = false

function NetAttribute:new(Name)
	
	return setmetatable( {}, NetAttribute )
	
end

function NetAttribute:GetInstance(netObject)
	
	return NetAttributeInstance:new(self, netObject)
	
end

function NetAttribute:SetDelay(Delay)
	
	self.Delay = Delay
	
	return self
	
end

function NetAttribute:GetDelay()
	
	return self.Delay
	
end

function NetAttribute:SetChannel(Channel)
	
	self.Channel = Channel
	
	return self
	
end

function NetAttribute:GetChannel()
	
	return self.Channel
	
end

function NetAttribute:SetReliable(Reliable)
	
	self.Reliable = Reliable
	
	return self
	
end

function NetAttribute:GetReliable()
	
	return self.Reliable
	
end

function NetAttribute:SetSequenced(Sequenced)
	
	self.Sequenced = Sequenced
	
	return self
	
end

function NetAttribute:GetSequenced()
	
	return self.Sequenced
	
end

function NetAttribute:SetProtected(Protected)
	
	self.Protected = Protected
	
	return self
	
end

function NetAttribute:GetProtected()
	
	return self.Protected
	
end

function NetAttribute:SetSetter(Setter)
	
	self.Setter = Setter
	
	return self
	
end

function NetAttribute:SetGetter(Getter)
	
	self.Getter = Getter
	
	return self
	
end

function NetAttribute:SetSetterName(SetterName)
	
	self.SetterName = SetterName
	
	return self
	
end

function NetAttribute:GetSetterName()
	
	return self.SetterName
	
end

function NetAttribute:SetGetterName(GetterName)
	
	self.GetterName = GetterName
	
	return self
	
end

function NetAttribute:GetGetterName()
	
	return self.GetterName
	
end

function NetAttribute:Set(Object, ...)
	
	if self.Setter then
		
		return self.Setter(Object, ...)
		
	elseif self.SetterName then
		
		return Object[self.SetterName](Object, ...)
		
	end
	
	Object[self.Name] = ...
	
end

function NetAttribute:Get(Object)
	
	if self.Getter then
		
		return self.Getter(Object)
		
	elseif self.GetterName then
		
		return Object[self.GetterName](Object)
		
	end
	
	return Object[self.Name]
	
end

return NetAttribute