module("onet.MessageEncoder", package.seeall)

Object				= require("onet.Object")
MessageType			= require("onet.MessageType")
Serializer			= require("onet.serializer.Serializer")

MessageEncoder = setmetatable( {}, Object )
MessageEncoder.__index = MessageEncoder
MessageEncoder.__type = "MessageEncoder"

function MessageEncoder:new(ServerObj)
	
	local self = setmetatable( {}, MessageEncoder )
	
	self.Server 		= ServerObj
	self.Serializer	= Serializer:new(self)
	self.BooleanSerializer 	= self.Serializer:GetSerializer("BooleanSerializer")
	self.NumberSerializer	= self.Serializer:GetSerializer("NumberSerializer")
	self.StringSerializer	= self.Serializer:GetSerializer("StringSerializer")
	
	return self
	
end

function MessageEncoder:SendObject(Peer, Object)
	-- Cannot send remote objects
	if not Object:onetGetRemote() then
		
		if Object:onetGetID() == nil then
			
			local ObjectManager = self.Server:GetObjectManager()
			local ID = ObjectManager:GetAssignableID()
			
			Object:onetSetID( ID )
			ObjectManager:SetObject(ID, Object)
			
		end
		
		local ID = Object:onetGetID()
		
		if ID then
			
			self.Serializer:SetTargetPeer(Peer)
			
			local Values 			= Object:onetGetAllValues()
			local SerialType		= self.StringSerializer:Serialize(Object:type())
			local SerialID			= self.NumberSerializer:Serialize(ID)
			local SerialValues 	= self.Serializer:Serialize(Values)
			local Message 			= SerialType .. SerialID .. SerialValues
			
			Peer:Send(
				string.char(MessageType.CREATE_OBJECT) .. Message,
				Object:onetGetCreateChannel(),
				Object:onetGetCreateReliable(),
				Object:onetGetCreateSequenced()
			)
			
			return true
			
		end
		
	end
	
	return false
	
end

function MessageEncoder:UpdateObject(Peer, Object, Changes, Channel, Reliable, Sequenced)
	
	local ID = Object:onetGetID()
	
	if ID then
		
		local Remote = Object:onetGetRemote()
		
		if not Remote or Remote == Peer then
			
			self.Serializer:SetTargetPeer(Peer)
			
			local SerialRemote	= self.BooleanSerializer:Serialize( Remote ~= nil and Remote ~= false )
			local SerialID			= self.NumberSerializer:Serialize(ID)
			local SerialChanges	= self.Serializer:Serialize(Changes)
			local Message			= SerialRemote .. SerialID .. SerialChanges
			
			Peer:Send( string.char(MessageType.UPDATE_OBJECT) .. Message, Channel, Reliable, Sequenced )
			
		end
		
	end
	
end

function MessageEncoder:DestroyObject(Peer, Object)
	
	local ID = Object:onetGetID()
	
	if ID then
		
		if not Object:onetGetRemote() then
			
			self.Serializer:SetTargetPeer(Peer)
			
			Peer:Send(
				string.char(MessageType.DESTROY_OBJECT) .. self.NumberSerializer:Serialize(ID),
				Object:onetGetDestroyChannel(),
				Object:onetGetDestroyReliable(),
				Object:onetGetDestroySequenced()
			)
			
		end
		
	end
	
end

function MessageEncoder:CallMethod(Peer, Object, MethodName, Channel, Reliable, Sequenced, ...)
	
	local ID = Object:onetGetID()
	
	if ID then
		
		local Remote = Object:onetGetRemote()
		
		if not Remote or Remote == Peer then
			
			self.Serializer:SetTargetPeer(Peer)
			
			local SerialID			= self.NumberSerializer:Serialize(ID)
			local SerialRemote	= self.BooleanSerializer:Serialize(Remote ~= nil and Remote ~= false)
			local SerialMethod	= self.StringSerializer:Serialize(MethodName)
			local SerialParams	= self.Serializer:Serialize({...})
			local Serial			= SerialID .. SerialRemote .. SerialMethod .. SerialParams
			
			Peer:Send(
				string.char(MessageType.CALL_METHOD) .. Serial,
				Channel,
				Reliable,
				Sequenced
			)
			
		end
		
	end
	
end

return MessageEncoder