module("onet.Table", package.seeall)

NetObject = require("onet.NetObject")
Change = require("onet.Change")
PeerList = require("onet.PeerList")

Table = setmetatable( {}, NetObject )
Table.__index = Table
Table.__type = "Table"
Table.__gc = NetObject.__gc

Table.onetProtected = {
	["onetValue"] = true,
	["onetPeers"] = true,
	["onetRemote"] = true,
	["onetID"] = true,
	["onetAttributeChannel"] = true,
	["onetAttributeReliable"] = true,
	["onetAttributeSequenced"] = true,
	["onetAttributeProtected"] = true,
	["onetCreateChannel"] = true,
	["onetCreateReliable"] = true,
	["onetCreateSequenced"] = true,
	["onetDestroyChannel"] = true,
	["onetDestroyReliable"] = true,
	["onetDestroySequenced"] = true,
}

Table.onetAttributeChannel = 0
Table.onetAttributeReliable = false
Table.onetAttributeSequenced = false
Table.onetAttributeProtected = false

function Table:onetInit()
	
	self.onetValue = {}
	self.onetPeers = PeerList:new()
	--self.onetRemote = nil
	--self.onetID = nil
	
end

function Table:onetGetAllValues()
	
	local Values = {}
	
	for Index, Value in pairs(self) do
		
		if not self.onetProtected[Index] then
			
			Values[Index] = Value
			
		end
		
	end
	
	return Values
	
end

function Table:onetSetValues(Values)
	
	for Index, Value in pairs(Values) do
		
		if not self.onetProtected[Index] then
			
			self.onetValue[Index] = Value
			
		end
		
	end
	
end

function Table:onetFetchChanges()
	
	local Changes = {}
	
	for Index, Value in pairs(self.onetValue) do
		
		if self[Index] == nil then
			
			Changes[Index] = Change:new(nil)
			
		end
		
	end
	
	for Index, Value in pairs(self) do
		
		if not self.onetProtected[Index] then
			
			if Value ~= self.onetValue[Index] then
				
				Changes[Index] = Change:new(Value)
				
			end
			
		end
		
	end
	
	return Changes
	
end

function Table:onetUpdateChanges(Changes)
	
	for Index, Change in pairs(Changes) do
		
		self.onetValue[Index] = Change:GetValue()
		
	end
	
end

function Table:onetReceiveChanges(Changes)
	
	if not self:onetGetAttributeProtected() then
		
		for Index, Change in pairs(Changes) do
			
			if not self.onetProtected[Index] then
				
				self.onetValue[Index] 	= Change:GetValue()
				self[Index]					= Change:GetValue()
				
			end
			
		end
		
	end
	
end

function Table:onetAllowAccess(Peer)
	
	if not self.onetRemote then
		
		self.onetPeers:Put(Peer)
		
	end
	
end

function Table:onetDisallowAccess(Peer)
	
	if not self.onetRemote then
		
		self.onetPeers:Remove(Peer)
		
	end
	
end

function Table:onetMethod()
	-- Do nothing
end

function Table:onetRequestCallMethod()
	-- Do nothing
end

function Table:onetSetRemote(Remote)
	
	self.onetRemote = Remote
	
end

function Table:onetGetRemote()
	
	return self.onetRemote
	
end

function Table:onetSetID(ID)
	
	self.onetID = ID
	
end

function Table:onetGetID()
	
	return self.onetID
	
end

function Table:onetSetCreateChannel(CreateChannel)
	
	self.onetCreateChannel = CreateChannel
	
end

function Table:onetGetCreateChannel()
	
	return self.onetCreateChannel
	
end

function Table:onetSetCreateReliable(CreateReliable)
	
	self.onetCreateReliable = CreateReliable
	
end

function Table:onetSetCreateSequenced(CreateSequenced)
	
	self.onetCreateSequenced = CreateSequenced
	
end

function Table:onetSetDestroyChannel(DestroyChannel)
	
	self.onetDestroyChannel = DestroyChannel
	
end

function Table:onetSetDestroyReliable(DestroyReliable)
	
	self.onetDestroyReliable = DestroyReliable
	
end

function Table:onetSetDestroySequenced(DestroySequenced)
	
	self.onetDestroySequenced = DestroySequenced
	
end

function Table:onetSetObjectManager(ObjectManager)
	
	self.onetObjectManager = ObjectManager
	
end

function Table:onetSetAttributeChannel(AttributeChannel)
	
	self.onetAttributeChannel = AttributeChannel
	
end

function Table:onetGetAttributeChannel()
	
	return self.onetAttributeChannel
	
end

function Table:onetSetAttributeReliable(Reliable)
	
	self.onetAttributeReliable = Reliable
	
end

function Table:onetGetAttributeReliable()
	
	return self.onetAttributeReliable
	
end

function Table:onetSetAttributeSequenced(Sequenced)
	
	self.onetAttributeSequenced = Sequenced
	
end

function Table:onetGetAttributeSequenced()
	
	return self.onetAttributeSequenced
	
end

function Table:onetSetAttributeProtected(Protected)
	
	self.onetAttributeProtected = Protected
	
end

function Table:onetGetAttributeProtected()
	
	return self.onetAttributeProtected
	
end

return Table