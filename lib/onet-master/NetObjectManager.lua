module("onet.NetObjectManager", package.seeall)

Object 		= require("onet.Object")
NetObject	= require("onet.NetObject")
Util			= require("onet.Util")
Table			= require("onet.Table")
table 		= Util.table

NetObjectManager = setmetatable( {}, Object )
NetObjectManager.__index = NetObjectManager
NetObjectManager.__type = "NetObjectManager"

NetObjectManager.MaxID = 2 ^ 24
NetObjectManager.Remote = false

--WeakKeys = {__mode = "k"}
--WeakValues = {__mode = "v"}

function NetObjectManager:new()
	
	local self = setmetatable( {}, NetObjectManager )
	
	self.TypeRegistry 	= {
		["Table"] = Table
	}
	self.Object				= {} --setmetatable( {}, WeakValues )
	self.ObjectID			= {} --setmetatable( {}, WeakKeys )
	self.ID					= 0
	
	self.RecvQueue			= {}
	
	return self
	
end

function NetObjectManager:GetRecvObject()
	
	local Index, Object = next(self.RecvQueue)
	
	if Object then
		
		self.RecvQueue[Index] = nil
		
	end
	
	return Object
	
end

function NetObjectManager:GetAssignableID()
	
	if self.Object[self.ID + 1] or self.ID + 1 > self.MaxID then
		
		self.ID = #self.Object + 1
		
	else
		
		self.ID = self.ID + 1
		
	end
	
	if self.ID >= 0 and self.ID <= self.MaxID then
		
		return self.ID
		
	end
	
end

function NetObjectManager:SetRemote(Remote)
	
	self.Remote = Remote
	
end

function NetObjectManager:GetRemote()
	
	return self.Remote
	
end

function NetObjectManager:SetRecvQueue(RecvQueue)
	
	self.RecvQueue = RecvQueue
	
end

function NetObjectManager:GetRecvQueue()
	
	return self.RecvQueue
	
end

function NetObjectManager:ReceiveObject(Type, ID, Values)
	
	local ObjectType			= self.TypeRegistry[Type]
	local Object				= ObjectType:onetNew(Values)
	
	if self.Object[ID] then
		
		Object = ObjectType:onetNewFrom( self.Object[ID] )
		
	else
		
		Object = ObjectType:onetNew(Values)
		
	end
	
	Object:onetSetID(ID)
	Object:onetSetRemote(self.Remote)
	Object:onetSetObjectManager(self)
	
	self.Object[ID] 			= Object
	self.ObjectID[Object]	= ID
	
	table.insert(self.RecvQueue, Object)
	
	return Object
	
end

function NetObjectManager:GetTempObject(ID)
	
	if not self.Object[ID] then
		
		local NetObjectObj = NetObject:new()
		
		NetObjectObj:onetSetID(ID)
		NetObjectObj:onetSetRemote(self.Remote)
		NetObjectObj:onetSetObjectManager(self)
		
		self.Object[ID] = NetObjectObj
		
	end
	
	return self.Object[ID]
	
end

function NetObjectManager:GetObject(ID)
	
	return self.Object[ID]
	
end

function NetObjectManager:SetObject(ID, Object)
	
	if Object then
		
		Object:onetSetID(ID)
		Object:onetSetRemote(self.Remote)
		Object:onetSetObjectManager(self)
		
		self.Object[ID] 			= Object
		self.ObjectID[Object] 	= ID
		
	else
		
		self:RemoveObject(ID)
		
	end
	
end

function NetObjectManager:RemoveObject(ID)
	
	self.Object[ID] = nil
	table.removeValue(self.ObjectID, ObjectID)
	
end

function NetObjectManager:GetObjectEnumerator()
	
	return next, self.Object
	
end

function NetObjectManager:SetTypeRegistry(TypeRegistry)
	
	self.TypeRegistry = TypeRegistry
	
end

function NetObjectManager:GetTypeRegistry()
	
	return self.TypeRegistry
	
end

function NetObjectManager:RegisterType(NetObject)
	
	self.TypeRegistry[ NetObject:type() ] = NetObject
	
end

return NetObjectManager