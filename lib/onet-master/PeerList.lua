module("onet.PeerList", package.seeall)

Object = require("onet.Object")

PeerList = setmetatable( {}, Object )
PeerList.__index = PeerList
PeerList.__type = "PeerList"

function PeerList:new()
	
	return setmetatable( {}, PeerList )
	
end

function PeerList:Put(Peer)
	
	self[ Peer:GetAddress() ] = Peer
	
end

function PeerList:Remove(Peer)
	
	self[ Peer:GetAddress() ] = nil
	
end

function PeerList:Get(Address)
	
	return self[ Address ]
	
end

function PeerList:GetPeerEnumerator()
	
	return next, self
	
end

return PeerList