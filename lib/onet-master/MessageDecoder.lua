module("onet.MessageDecoder", package.seeall)

Object					= require("onet.Object")
Deserializer			= require("onet.deserializer.Deserializer")
MessageType				= require("onet.MessageType")

MessageDecoder = setmetatable( {}, Object )
MessageDecoder.__index = MessageDecoder
MessageDecoder.__type = "MessageDecoder"

function MessageDecoder:new(ServerObj)
	
	local self = setmetatable( {}, MessageDecoder )
	
	self.Server			= ServerObj
	
	self.Deserializer = Deserializer:new(self)
	self.BooleanDeserializer = self.Deserializer:GetDeserializer("BooleanDeserializer")
	self.NumberDeserializer = self.Deserializer:GetDeserializer("NumberDeserializer")
	self.StringDeserializer = self.Deserializer:GetDeserializer("StringDeserializer")
	
	return self
	
end

function MessageDecoder:GetServer()
	
	return self.Server
	
end

function MessageDecoder:Decode(Peer, Data)
	
	self.Deserializer:SetSourcePeer(Peer)
	
	local Byte, Data = Data:byte(1), Data:sub(2)
	
	if Byte == MessageType.CREATE_OBJECT then
		
		self:ReceiveObject(Peer, Data)
		
	elseif Byte == MessageType.UPDATE_OBJECT then
		
		self:UpdateObject(Peer, Data)
		
	elseif Byte == MessageType.DESTROY_OBJECT then
		
		self:DestroyObject(Peer, Data)
		
	elseif Byte == MessageType.CALL_METHOD then
		
		self:CallMethod(Peer, Data)
		
	end
	
end

function MessageDecoder:ReceiveObject(Peer, Data)
	
	local Type, ID, Values
	
	Type, 	Data	= self.StringDeserializer:Deserialize(Data)
	ID,		Data	= self.NumberDeserializer:Deserialize(Data)
	Values,	Data	= self.Deserializer:Deserialize(Data)
	
	if Type and ID and Values then
		
		Peer:GetObjectManager():ReceiveObject(Type, ID, Values)
		
	end
	
end

function MessageDecoder:UpdateObject(Peer, Data)
	
	local Remote, ID, Values
	
	Remote,	Data = self.BooleanDeserializer:Deserialize(Data)
	ID,		Data = self.NumberDeserializer:Deserialize(Data)
	Changes,	Data = self.Deserializer:Deserialize(Data)
	
	if ID and Changes then
		
		local Object
		
		if Remote then
			-- Remote for him, local to us
			Object = self.Server:GetObjectManager():GetObject(ID)
			
		else
			-- Local for him, remote to us
			Object = Peer:GetObjectManager():GetObject(ID)
			
		end
		
		if Object then
			
			Object:onetReceiveChanges(Changes)
			
		end
		
	end
	
end

function MessageDecoder:DestroyObject(Peer, Data)
	
	local ID, Data = self.NumberDeserializer:Deserialize(Data)
	
	if ID then
		
		Peer:GetObjectManager():RemoveObject(ID)
		
	end
	
end

function MessageDecoder:CallMethod(Peer, Data)
	
	local ID, Remote, Method, Params
	
	ID,		Data	= self.NumberDeserializer:Deserialize(Data)
	Remote,	Data	= self.BooleanDeserializer:Deserialize(Data)
	Method,	Data	= self.StringDeserializer:Deserialize(Data)
	Params,	Data	= self.Deserializer:Deserialize(Data)
	
	if ID and Method and Params then
		-- Remote for him, local to us
		local Object
		
		if Remote then
			
			Object = self.Server:GetObjectManager():GetObject(ID)
			
		else
			
			Object = Peer:GetObjectManager():GetObject(ID)
			
		end
		
		if Object then
			
			Object:onetRequestCallMethod(Method, Params)
			
		end
		
	end
	
end

return MessageDecoder