module("onet.NetMethod", package.seeall)

Object = require("onet.Object")

NetMethod = setmetatable( {}, Object )
NetMethod.__index = NetMethod
NetMethod.__type = "NetMethod"

NetMethod.Channel = 0
NetMethod.Reliable = true
NetMethod.Sequenced = true

function NetMethod:new()
	
	return setmetatable( {}, NetMethod )
	
end

function NetMethod:SetChannel(Channel)
	
	self.Channel = Channel
	
	return self
	
end

function NetMethod:GetChannel()
	
	return self.Channel
	
end

function NetMethod:SetReliable(Reliable)
	
	self.Reliable = Reliable
	
	return self
	
end

function NetMethod:GetReliable()
	
	return self.Reliable
	
end

function NetMethod:SetSequenced(Sequenced)
	
	self.Sequenced = Sequenced
	
	return self
	
end

function NetMethod:GetSequenced()
	
	return self.Sequenced
	
end

return NetMethod