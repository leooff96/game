module("onet.Server", package.seeall)

Object 				= require("onet.Object")

MessageDecoder		= require("onet.MessageDecoder")
MessageEncoder		= require("onet.MessageEncoder")
NetObjectManager 	= require("onet.NetObjectManager")
NetObjectUpdater	= require("onet.NetObjectUpdater")
Peer 					= require("onet.Peer")
PeerList 			= require("onet.PeerList")
enet 					= require("onet.enet")

Server = setmetatable( {}, Object )
Server.__index = Server
Server.__type = "Server"

function Server:new(Port)
	
	local self = setmetatable( {}, Server )
	local Port = Port or 0
	
	self.Host 				= enet.host_create("localhost:" .. Port)
	self.Address 			= self.Host:get_socket_address()
	self.Peers 				= PeerList:new()
	self.ObjectManager	= NetObjectManager:new()
	self.ObjectUpdater	= NetObjectUpdater:new(self)
	
	self.MessageDecoder	= MessageDecoder:new(self)
	self.MessageEncoder	= MessageEncoder:new(self)
	
	self:Log("Host started at '" .. self.Address .. "'")
	
	return self
	
end

function Server:Connect(Address)
	
	local enetPeer = self.Host:connect(Address)
	
	if enetPeer then
		
		local PeerObj = Peer:new( enetPeer )
		
		PeerObj:SetServer(self)
		PeerObj:GetObjectManager():SetTypeRegistry( self.ObjectManager:GetTypeRegistry() )
		
		self.Peers:Put( PeerObj )
		
		return PeerObj
		
	end
	
end

function Server:GetPeerList()
	
	return self.Peers
	
end

function Server:GetAddress()
	
	return self.Address
	
end

function Server:GetObjectManager()
	
	return self.ObjectManager
	
end

function Server:GetObjectUpdater()
	
	return self.ObjectUpdater
	
end

function Server:GetMessageEncoder()
	
	return self.MessageEncoder
	
end

function Server:GetMessageDecoder()
	
	return self.MessageDecoder
	
end

function Server:Update()
	
	self:UpdateEvents()
	
	self.ObjectUpdater:Update()
	self.Host:flush()
	
end

function Server:Service()
	
	return self.Host:service(1)
	
end

function Server:UpdateEvents()
	
	for Event in self.Service, self do
		
		local Address = tostring(Event.peer)
		
		if Event.type == "connect" then
			
			local PeerObj = self.Peers:Get( Address )
			
			if PeerObj then
				
				PeerObj:SetPeer( Event.peer )
				PeerObj:GetObjectManager():SetRecvQueue( self.ObjectManager:GetRecvQueue() )
				
				if not PeerObj:GetConnected() then
					
					PeerObj:OnConnect()
					self:Log( "'" .. PeerObj:GetAddress() .. "' connected at '" .. self:GetAddress() .. "'" )
					
				end
				
			else
				
				local PeerObj = Peer:new( Event.peer )
				
				PeerObj:SetServer(self)
				PeerObj:GetObjectManager():SetTypeRegistry( self.ObjectManager:GetTypeRegistry() )
				PeerObj:GetObjectManager():SetRecvQueue( self.ObjectManager:GetRecvQueue() )
				PeerObj:OnConnect()
				
				self.Peers:Put( PeerObj )
				self:Log( "'" .. PeerObj:GetAddress() .. "' connected at '" .. self:GetAddress() .. "'" )
				
			end
			
		elseif Event.type == "disconnect" then
			
			local PeerObj = self.Peers:Get( Address )
			
			if PeerObj then
				
				PeerObj:OnDisconnect()
				self.Peers:Remove( PeerObj )
				self:Log( "'" .. PeerObj:GetAddress() .. "' disconnected from '" .. self:GetAddress() .. "'" )
				
			end
			
		elseif Event.type == "receive" then
			
			local PeerObj = self.Peers:Get( Address )
			
			if PeerObj then
				
				PeerObj:OnReceive( Event.data )
				
			end
			
		end
		
	end
	
end

function Server:Log(...)
	
	print(...)
	
end

return Server