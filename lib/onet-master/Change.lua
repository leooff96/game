module("onet.Change", package.seeall)

Object = require("onet.Object")

Change = setmetatable( {}, Object )
Change.__index = Change
Change.__type = "Change"

function Change:new(Value)
	
	local self = setmetatable( {}, Change )
	
	self:SetValue(Value)
	
	return self
	
end

function Change:SetValue(Value)
	
	self.Value = Value
	
end

function Change:GetValue()
	
	return self.Value
	
end

function Change:SetAttribute(Attribute)
	
	self.Attribute = Attribute
	
end

function Change:GetAttribute()
	
	return self.Attribute
	
end

function Change:GetType()
	
	return type( self.Value )
	
end

return Change