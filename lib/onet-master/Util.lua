module("onet.Util", package.seeall)

local table = setmetatable( {}, {__index = table})

function table.contains(table, value)
	
	for k, v in pairs(table) do
		
		if v == value then
			
			return true
			
		end
		
	end
	
	return false
	
end

function table.indexOf(table, value)
	
	for k, v in pairs(table) do
		
		if v == value then
			
			return k
			
		end
		
	end
	
	return nil
	
end

function table.removeValue(table, value)
	
	for k, v in pairs(table) do
		
		if v == value then
			
			table[k] = nil
			break
			
		end
		
	end
	
end

return {
	table = table
}