require "source/Player"
require "source/Bullet"

local socket = require "socket"

local option = 0
local udp
local players = {}
local bullets = {}
local myPlayer = {}
local ip = ""
local menu = love.graphics.newImage("/assets/resources/menu.jpg")
local play = love.graphics.newImage("/assets/resources/play.png")
local grass = love.graphics.newImage("/assets/resources/grass2.jpg")
local msg = ""
function love.load()
  love.graphics.setFont(love.graphics.newFont(15))
  updaterate = 0.1
  t = 0
  thread = love.thread.newThread("source/Server.lua")
  thread:start()
  love.keyboard.setKeyRepeat(true)
  text = ""
  udp = socket.udp()
  udp:settimeout(0.001)
  --udp:setsockname("*",53474)

  myPlayer = Player:new()
  myPlayer.name = "YOU"
  --  players[0]= Player:new()
  --  players[0].x = 150
  --  players[0].y = 150
  --  players[0].angle = 45

  --  for key, var in pairs(players) do
  --  	var:load()
  --  end

  myPlayer:load()

  math.randomseed(os.time())
  entity = tostring(math.random(99999))
end

function love.draw()
  if (option == 3) then
    local width, height = menu:getDimensions()
    love.graphics.draw(menu, 0, 0, 0, 1, 1)
    love.graphics.printf("Server Connection: " .. text, width / 2 - 150, height / 2 + 30, width / 2, nil, 0, 1)
    love.graphics.printf("LOG: " .. msg, width / 2 - 150, height / 2 + 70, width / 2, nil, 0, 1)
  end

  if (option == 0) then
    -- love.graphics.setFont(love.graphics.newFont(18))
    --love.graphics.printf("Conectar",  width/2 -150, height/2 + 30, width/2 -150-300 ,'center', 0,2)
    --love.graphics.printf(text,  width/2 -150, height/2 + 30, width/2,nil, 0,1)
    --local width, height = love.graphics.getDimensions()
    local width, height = menu:getDimensions()
    love.graphics.draw(menu, 0, 0, 0, 1, 1)
    love.graphics.draw(play, width / 2, height / 2, 0, 1)
  elseif option == 1 then
    love.graphics.draw(grass, 0, 0, 0, 1, 1)
    for key, var in pairs(players) do
      var:draw()
    end
    for key, var in pairs(bullets) do
      var:draw()
    end
    myPlayer:draw()

    love.graphics.printf("LIFE: " .. myPlayer.life, 0, 0, love.graphics.getWidth())
  --love.graphics.setColor(0, 1, 0, 1)
  --love.graphics.print(data or 'teste', 10, 200)
  end
end

function love.update(dt)
  if (option == 0) then
  elseif option == 1 then
    myPlayer.angle = angleMouse(myPlayer)
    data, msg = udp:receive()

    if data then
      ent, cmd, parms = data:match("^(%S*) (%S*) (.*)")
      if ent ~= entity then
        if cmd == "move" then
          local x, y, anglex = parms:match("^(%S*) (%S*) (%S*)$")
          x, y, anglex = tonumber(x), tonumber(y), tonumber(anglex)
          if (players[ent] == nil) then
            players[ent] = Player:new()
            players[ent]:load()
          end
          players[ent].x = x
          players[ent].y = y
          players[ent].angle = anglex
          players[ent].entity = entity
        end
        if cmd == "shoot" then
          local x, y, anglex, mxx, myy = parms:match("^(%S*) (%S*) (%S*) (%S*) (%S*)$")
          x, y, anglex, mxx, myy = tonumber(x), tonumber(y), tonumber(anglex), tonumber(mxx), tonumber(myy)
          bullets[table.getn(bullets) + 1] = Bullet:new(x, y, anglex, mxx, myy, ent)
        end
        if cmd == "damage" then
          love.graphics.printf("damage: " .. myPlayer.life, 0, 15, love.graphics.getWidth())
          players[ent].life = players[ent].life - Bullet.damage
        end

        if cmd == "connect" then
          players[ent] = Player:new()
          players[ent]:load()
        end
      end
    end

    for key, var in pairs(players) do
      var:update(dt)
    end

    for key, var in pairs(bullets) do
      var:update(dt)
      love.graphics.printf("bullets: " .. var.entity, 0, 15, love.graphics.getWidth())
      if entity ~= var.entity then
        if (colisao(var, myPlayer)) then
            myPlayer.life = myPlayer.life - var.damage
            bullets[key] = nil
            local dg = string.format("%s %s", entity, "damage")
            udp:send(dg)
        end
--      else
--        for p, player in pairs(players) do
--          love.graphics.printf("verifica: " .. player.entity, 0, 15, love.graphics.getWidth())
--          if (colisao(var, player)) then
--            player.life = player.life - var.damage
--            bullets[key] = nil
--            local dg = string.format("%s %s", player.entity, "damage")
--            udp:send(dg)
--            love.graphics.printf("colisao: " .. player.entity, 0, 15, love.graphics.getWidth())
--          end
--        end
      end

      if var:isEnd() then
        bullets[key] = nil
      end
    end

    local isMove = myPlayer:move()
    myPlayer:update(dt)

    t = t + dt -- increase t by the deltatime
    if t > updaterate then
      local dg = string.format("%s %s %f %f %f", entity, "move", myPlayer.x, myPlayer.y, myPlayer.angle)
      udp:send(dg)
      t = t - updaterate
    end
  end
end

function love.keypressed(key, isrepeat)
  if (option == 3) then
    if key == "backspace" then
      text = string.sub(text, 1, -2)
    end

    if key == "escape" then
      option = 0
    end

    if key == "return" then
      ip = text
      if (udp:setpeername(ip, 53474)) then
        local dg = string.format("%s %s $", entity, "connect")
        udp:send(dg)
        option = 1
        msg = " Connectando..."
      else
        msg = " Não conectado"
      end
    end
  end

  --  if key == "k" then
  --     ip = text
  --     option = 1
  --     assert(udp:setpeername(ip,53474))
  --     local dg = string.format("%s %s $", entity, 'connect')
  --    udp:send(dg)
  --  end
end

function love.keyreleased(key)
end

function love.textinput(t)
  text = text .. t
end

function love.mousepressed(x, y, button)
end

function love.mousemoved(x, y, dx, dy)
end

function love.mousereleased(x, y, button)
  if (option == 0) then
    if (button == 1) then
      local width, height = menu:getDimensions()
      local widthp, heightp = play:getDimensions()

      if x > width / 2 and x < width / 2 + widthp and y < height / 2 + heightp and y > height / 2 then
        --text = text .. 'click'
        option = 3
      end
    end
  elseif option == 1 then
    if (button == 1) then
      bullets[table.getn(bullets) + 1] = Bullet:new(myPlayer.x, myPlayer.y, myPlayer.angle, nil, nil, entity)
      local mxx, myy = love.mouse.getPosition()
      local dg =
        string.format("%s %s %f %f %f %f %f", entity, "shoot", myPlayer.x, myPlayer.y, myPlayer.angle, mxx, myy)
      udp:send(dg)
    end
  end
end

function angleMouse(o)
  mx, my = love.mouse.getPosition()
  return math.atan2(my - o.y, mx - o.x)
end

function colisao(obj, obj2)
  return obj.x < obj2.x + 50 and obj2.x < obj.x + 50 and obj.y < obj2.y + 50 and obj2.y < obj.y + 50
end

function colisao2(obj, obj2)
  return obj.x < obj2.x and obj2.x < obj.x and obj.y < obj2.y and obj2.y < obj.y
end
