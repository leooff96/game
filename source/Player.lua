Player = {
  x = 0,
  y = 0,
  w = 313,
  h = 206,
  angle = 0,
  speed = 1,
  spriteMove = {},
  pos = 0,
  scale = 0.5,
  life = 100,
  entity = 0,
  name=''
}

Player.__index = Player 
function Player:new()
   local self = setmetatable({}, Player)
   return self
end

function Player:load()
    self.spriteMove = love.graphics.newImage(string.format("/assets/player/rifle/move/survivor-move_rifle_%d.png",0))
    self.w, self.h = self.spriteMove:getDimensions()
end

function Player:draw()
  --love.graphics.printf(self.angle, 0, 0, love.graphics.getWidth())
 
  love.graphics.draw(self.spriteMove, self.x, self.y, self.angle, self.scale, self.scale, self.w/2 ,self.h/2)

   love.graphics.printf(self.name, self.x,  self.y-5,3000,nil, 0,1)
end

function Player:update(dt)

end

function Player:move()
    local move = false
    if(love.keyboard.isDown("d")) then
      self.x = self.x + self.speed
      self.pos = self.pos + 1
      --self.posFeet = self.posFeet + 1
      move = true
      if(self.pos > 17)then
        self.pos = 0
       -- self.posFeet = 0
      end
      
    end
    if(love.keyboard.isDown("a")) then
      self.x = self.x - self.speed     
      self.pos = self.pos - 1
     -- self.posFeet = self.posFeet - 1
            move = true
      if(self.pos < 0)then
        self.pos = 16
       -- self.posFeet = 16
      end
      
    end
    if(love.keyboard.isDown("s")) then
      self.y = self.y + self.speed
            move = true
    end
    if(love.keyboard.isDown("w")) then
      self.y = self.y - self.speed
            move = true
    end
    return  move
end