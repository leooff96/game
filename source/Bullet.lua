tween = require "lib/tween"
Bullet = {
  img = {},
  idPlayer=0,
  x = 0,
  y = 0,
  w = 25,
  h = 15,
  scale=1,
  angle= 0,
  mx = 0,
  my = 0,
  damage = 35,
  pos = {},
  entity=0
}
Bullet.__index = Bullet 

function Bullet:load()

end

function Bullet:new(x,y,angle,mx,my,entity)
  
   local self = setmetatable({}, Bullet)
   self.x = x
   self.y = y
   self.angle = angle
   self.entity= entity
   
    self.img  = love.graphics.newImage("/assets/resources/images/bullet.png")
   if not mx and not my then
    mx,my = love.mouse.getPosition()
   end
   
    self.mx = mx
    self.my = my
    self.pos = tween.new(0.5, self, {x=mx,y=my},'linear')
   
   return self
end

function Bullet:update(dt)
  self.pos:update(dt)
end

function Bullet:isEnd()
  return self.x == self.mx and self.y == self.my
end

function Bullet:draw()
    love.graphics.draw(self.img, self.x, self.y,self.angle,1,1,0,0) 
end
